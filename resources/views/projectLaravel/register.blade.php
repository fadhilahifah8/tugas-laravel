<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman 2</title>
</head>
<body>

    <h1>Buat Account Baru!</h1> 
    <h2>Sign Up Form</h2>
    <form action="/post" method="POST">
        @csrf 
        <label>First name:</label> <br><br>
    <input type="text" name="nama"> <br><br>
    <label>Last name:</label> <br><br>
    <input type="text" name="nama"> <br><br>
    <label>Gender</label> <br><br>
    <input type="radio"> &nbsp; Male <br>
    <input type="radio"> &nbsp; Female <br>
    <input type="radio"> &nbsp; Other <br><br>
    <label>Nationality:</label> <br><br>
    <select name="asal">
        <option value="1"> Indonesian </option>
        <option value="2"> American </option>
        <option value="3"> African </option>
        <option value="4"> Australian </option>
    </select> <br><br>
    <label>Languange Spoken:</label> <br><br>
    <input type="checkbox"> &nbsp; Bahasa Indonesia <br>
    <input type="checkbox"> &nbsp; English <br>
    <input type="checkbox"> &nbsp; Other <br><br>
    <label>Bio</label> <br><br>
    <textarea cols="40" rows="10"></textarea><br>
    <button>Sign Up</button>
    </form>
</body>
</html>