<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

// Route::get('/test/{angka}', function ($angka) {
//     return view ('test', ["angka" => $angka]);
// });

// Route::get('/halo/{nama}', function ($nama) {
//     return "Hallo $nama";
// });

 //Route::get('/form', 'IfadhController@form');
 //Route::post('/sapa', 'IfadhController@sapa_post');

 //Route::get('/form2', 'Form2Controller@form2');
 //Route::post('/post', 'Form2Controller@kirim');


Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/post', 'AuthController@sent');



