<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('projectLaravel.register');
    }

    public function sent(Request $request){
        $nama = $request->nama;
         return view('projectLaravel.welcome', compact('nama'));

    }
}
