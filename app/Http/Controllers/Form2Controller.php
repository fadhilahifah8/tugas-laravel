<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Form2Controller extends Controller
{
    public function form2(){
        return view('form2');
    }

    public function kirim(Request $request){
        //dd($request);
        $fullname = $request->fullname;
        $alamat = $request->address;
        return view('selamat', compact('fullname', 'alamat'));

    }
}
